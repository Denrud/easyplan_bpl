let infoSection = document.querySelector('.info-section div')
let h2 = document.createElement('h2')
let p = document.createElement('p')
let firstTitle = "Add Thermostat"
let secondTitle = "Place Thermostat"
let firstDescription = "You need this thermostat kit to connect to the 1st mat and to a socket. The kit also has a wired floor sensor. With Veria Wireless Clickkit you will be able to control the temperature quickly and precisely. You may also program 4 different time zones during the weekdays and weekend."
let secondDescription = "Drag the thermostat along the edges of the room, so it matches the placement in the actual room."
let buttonContinue = document.getElementById('btn-continue-thermostat')
let buttonWireles = document.getElementById('select-handler')
let infoAreaText = document.querySelector(".info-area-thermostat")
let productBox = document.querySelector(".product-box")

h2.innerText = firstTitle
p.innerText = firstDescription
infoSection.appendChild(h2)
infoSection.appendChild(p)

buttonWireles.addEventListener('click', () => {
    h2.innerText = secondTitle
    p.innerText = secondDescription
    infoAreaText.style.display = "none"
    buttonContinue.style.visibility = "visible"
    productBox.style.display = "none"
})